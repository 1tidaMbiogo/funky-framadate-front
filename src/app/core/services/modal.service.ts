import { ComponentType } from '@angular/cdk/portal';
import { Injectable, TemplateRef } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';

@Injectable({
	providedIn: 'root',
})
export class ModalService {
	constructor(public dialog: MatDialog) {}

	public openModal_OLD<T, K>(componentOrTemplateRef: ComponentType<T> | TemplateRef<T>, data?: K): void {
		this.dialog.open(componentOrTemplateRef, { data: data });
	}
	public openModal<T, D = any>(
		componentOrTemplateRef: ComponentType<T> | TemplateRef<T>,
		config?: MatDialogConfig<D>
	): void {
		this.dialog.open<T, D>(componentOrTemplateRef, config);
	}
}
