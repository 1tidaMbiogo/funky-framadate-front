# Guide utilisateur
## Principe
Framadate permet de créer et répondre à des sondages demandant des disponibilité dans le temps à plusieurs personnes afin de détermnier un consensus pour organiser des rendez-vous.
Il faut donc qu'une personne crée un sondage et publie un lien aux personnes qu'elle souhaite réunir.

## Glossaire

| Terme       | Signification                                               |
| :---        | :---                                                        |
| Créateur    | Personne ayant créé le sondage                              |
| Participant | Personne invitée à participer ou ayant participé            |
| Archivage   | Empêcher votes et commentaires mais conserver les résultats |
| _Slug_      | Partie le l'URL identifiant un sondage de manière unique    |


## Fonctionnalités actuellement dans framadate legacy et à conserver

### Généralités

Il existe deux types de sondage. Le type « dates » est adapté à la proposition de dates (voir [§plages horaires](#plages-horaires)) et le type « texte », plus généraliste, ne contient que du texte. Ce choix est l'un des premiers choix effectués lors de la création d'un nouveau sondage.

Deux types d'acteurs sont distingués : la personne ayant créé le sondage et les personnes qui répondent au sondage. Ces deux types d'acteurs n'ont pas les mêmes droits sur le sondage : la personne ayant créé le sondage est la seule ayant des droits d'administration sur celui-ci, en plus des droits des participants. En particulier, les droits d'administration permettent d'ajuster les droits des participants.


### Création d'un sondage

Il est nécessaire de fournir un email lors de la création du sondage. Cela permet aux créateurs de sondages de pouvoir obtenir via email la liste des sondages qu'ils ont créés.

Il est possible de choisir le _slug_ dans l'URL du sondage, à condition que ce _slug_ soit disponible.

Le sondage reste modifiable après sa création.


### Plages horaires

Les sondages de type « dates » permettent de proposer des jours et des plages horaires pour chaque journée.

Pour faciliter la saisie des dates proposées, il est demandé si les plages horaires sont les mêmes chaque jour ou si elles sont différentes selon les jours. Dans le premier cas, les plages horaires sont demandées uniquement pour le premier jour et sont automatiquement reproduites sur les autres jours.

Toujours pour faciliter la saisie des propositions, il est possible d'ajouter plusieurs jours consécutifs en sélectionnant le premier et le dernier jour plutôt qu'en sélectionnant chaque jour un à un.


## Participation à un sondage

### Votes

Les participants n'ont pas besoin de créer un compte ou de fournir leur email pour participer à un sondage.

Le créateur du sondage peut protéger le sondage par un mot de passe. Sans ce mot de passe, il est impossible de voir le sondage, donc impossible d'y voter. Attention cependant, le sondage est stocké en clair dans la base de données et ne bénéficie donc d'aucun chiffrement. *# Le mot de passe, lui, est-il chiffré ?*

Le créateur du sondage choisit de permettre ou non les modifications des votes sur le sondage. Il a le choix entre les trois formules suivantes :

* ne pas permettre de modifier de réponse ;
* permettre de modifier uniquement sa propre réponse a un sondage (modalités d'identification encore non déterminées) ;
* permettre de modifier toute réponse à un sondage (y compris celles des autres).


### Commentaires

Les participants au sondage ont la possibilité de créer des commentaires sur le sondage. Seule la personne ayant les droits d'administration sur le sondage peut modifier les commentaires.


## Résultats

Selon la configuration du sondage, les résultats peuvent n'être accessibles qu'à la personne ayant un accès d'administration ou bien être publics. Les personnes pouvant accéder aux résultats du sondage peuvent exporter ces résultats au format CSV.


## Emails

Lorsqu'une personne crée un sondage, un email reprenant les informations du sondage ainsi que les URL uniques servant à le modifier (lien d'administration) et à y participer (lien à transmettre aux participants) lui est envoyé. De plus, elle peut choisir de recevoir des emails lors d'un nouveau vote ou commentaire sur le sondage.

Une personne ayant créé au moins un sondage peut demander à recevoir par email la liste des sondages qu'elle a créés en utilisant cette adresse email.


## Stockage et export de données

Les données sont stockées en clair sur le serveur. L'archivage (resp. suppression) automatique des sondages est effectué via cronjob 90 (resp. 120) jours après sa création. Il est possible pour l'administrateur d'un sondage de modifier la durée avant archivage et la suppression automatique suit toujours de 30 jours la date d'archivage.

L'export d'un sondage et des résultats d'un sondage est possible au format CSV ainsi que JSON.


# Nouveautés dans Framadate Funky
Plus de détails dans la section [Historique](../historique.md) pour connaître le cheminement des versions précédentes.
Les idées et rapports de bugs sont disponibles dans [le tableau des Issues](https://framagit.org/framasoft/framadate/funky-framadate-front/-/boards) sur la forge logicielle.

## principales

* Accessibilité renforcée.
* Traduction dynamique de toutes les phrases en choisissant la langue dans le menu.
* Adapté aussi bien sur mobile que grands écrans à haute ou faible densité de pixels.
* Anti-spam de commentaires.
* Anti-spam de vote.
* Tests unitaires et end-to-end.
* Couverture de test.


## Nouveautés secondaires

* Choix de réponses possibles. Proposer de ne répondre que « oui » ou rien, ou aller dans la nuance en proposant « oui », « peut-être », « non », « ? ». *# Redondance ou le choix de réponses possibles de la première phrase concerne un autre choix ?*
* Insertion d'images dans le sondage de type texte, avec des URL uniquement. Une seule image par title possible ou rien.
* Thème sombre.
* Duplication de sondage à partir d'un autre.
* Boutons pour copier dans le presse-papier les liens publics et privés / admin des sondages.
* Limiter le nombre de participants maximum


# Idées pour de futures améliorations (pertinence à vérifier)

* Gagner en vie privée en chiffrant certaines informations ? Stockage zéro knowledge avec un chiffrement robuste à courbe elliptique.
* À réfléchir : permettre à Framadate de faire entrer à des gens plusieurs plages de temps de disponibilité et le service déduit quelles sont les plages de temps favorables (calcul d'intersection sur des lignes discontinues). Cela pourrait être avec divers niveaux de détail. Comme https://omnipointment.com/ (qui est un logiciel privateur)
* SSO du fédiverse ?
